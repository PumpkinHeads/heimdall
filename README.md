# README #

API sample for the Pervasive Computing course, Engigneering & Computer Science @ UNIBO, a.y. 2017-2018.

Heimdall will be a smart Telescope remotely controllable.
It will allow to take different pictures of points of the sky with different filters:
	
	* RGB (default)
	* UV
	* IR
	
It will have different motors controllable manually through the specification of the Yaw and Pitch.
Further It will permit also to specify a trajectory, that the telescope will follow moving autonomously in a specified time.
The trajectory will be a sequence of orientations and the relative permanence time.   
In order to be aware of the cardinal point it will be rotated, Heimdall is equipped with a magnetometer.

The API function are:	

	* Get the last image recorded
	* Get the last image recorded with a specified filter
	* Get an image take specifing the date
	* Get the current value of Yaw and Pitch
	* Set the value of Yaw and Pitch that the system must reach
	* Set the trajectory that the telescope must follow. 
	* Get the current cardinal poit orientation
 
The Api showed is under construction, at this moment the behaviour is simulated.
The Api could be founded at SwaggerHub repo: https://swaggerhub.com/apis/ps1718/Heimdall/1.0.0

	* Version: 1.0.0
	* Matteo Gabellini (matteo.gabellini2@studio.unibo.it)
	* Alessandro Cevoli (alessandro.cevoli@studio.unibo.it)


